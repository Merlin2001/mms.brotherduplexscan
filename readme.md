# Purpose
This tool helps you easily scan full-duplex using a brother MFC-J5320DW with automated document feeder (ADF). Since the printer's ADF can't turn the pages, you will have to scan two batches.

# How to
1. Use ADF to scan one side to PDF
2. Put the paper back into the ADF **without turning/rotating** it in any way
3. Scan the stack to PDF again
4. Use this tool to combine both PDF files into one (the default settings work if you followed the steps above, but feel free to experiment if you _did_ rotate the stack ;-))

# How it works
The tool uses a PDF library to interleave both PDF files while also rotating all pages of the second PDF.

There are obviously other tools out there that can do these steps (manually) but I just wanted a quick way to deal with all my duplex PDFs :-)