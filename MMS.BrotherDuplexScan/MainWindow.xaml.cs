﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Microsoft.Win32;

using MMS.BrotherDuplexScan.Properties;

namespace MMS.BrotherDuplexScan
{
   /// <summary>
   /// Interaktionslogik für MainWindow.xaml
   /// </summary>
   public partial class MainWindow : Window
   {
      public MainWindow()
      {
         InitializeComponent();

         lblStatus.Content = "Herzlich willkommen!   :-)";
      }

      private void btnProcess_Click( object sender, RoutedEventArgs e )
      {
         var combiner = new PdfCombiner();
         combiner.FrontPagesPdf = txtFileFrontPages.Text;
         combiner.BackPagesPdf = txtFileBackPages.Text;
         combiner.OutputFilePath = txtOutputFile.Text;
         combiner.RotateBackPages = chkRotateBackPages.IsChecked ?? false;
         combiner.DeleteSourceFiles = chkDeleteSourceFiles.IsChecked ?? false;

         if( File.Exists( combiner.OutputFilePath ) )
         {
            if( MessageBoxResult.Yes != MessageBox.Show( "Ausgabedatei existiert bereits.\n\nÜberschreiben?",
               "Datei überschreiben?", MessageBoxButton.YesNo, MessageBoxImage.Question ) )
            {
               return;
            }
         }

         bool success = false;
         try
         {
            combiner.SaveCombinedPdf();
            success = true;
         }
         catch( Exception ex )
         {
            success = false;
            MessageBox.Show( $"Ups, da ist leider was schiefgegangen:\n\n{ex.Message}",
                              "Fehler beim Speichern",
                              MessageBoxButton.OK,
                              MessageBoxImage.Warning );
         }
         finally
         {
            if( success )
            {
               lblStatus.Content = $"Jippie! PDF erfolgreich gespeichert   :-D";
            }
            else
            {
               lblStatus.Content = $"Ups, da ist was schiefgegangen   :-(";
            }
         }
      }

      private void btnSelectFrontPagesFile_Click( object sender, RoutedEventArgs e )
      {
         ShowFileOpenDialog( txtFileFrontPages, "Vorderseiten-PDF" );
      }
      private void btnSelectBackPagesFile_Click( object sender, RoutedEventArgs e )
      {
         ShowFileOpenDialog( txtFileBackPages, "Rückseiten-PDF" );
      }

      private void ShowFileOpenDialog( TextBox targetTextBox, string pdfType )
      {
         var fileOpenDialog = new OpenFileDialog();
         fileOpenDialog.Title = $"Bitte {pdfType} wählen...";
         fileOpenDialog.Filter = "pdf-Dateien|*.pdf|Alle Dateien|*.*";
         fileOpenDialog.Multiselect = false;
         fileOpenDialog.CheckPathExists = true;
         if( fileOpenDialog.ShowDialog() == true )
         {
            targetTextBox.Text = fileOpenDialog.FileName;
            lblStatus.Content = $"{pdfType} gewählt";
         }
         else { lblStatus.Content = "Auswahl abgebrochen"; }
      }

      private void btnSelectOutputFile_Click( object sender, RoutedEventArgs e )
      {
         var fileSaveDialog = new SaveFileDialog();
         fileSaveDialog.DefaultExt = ".pdf";
         fileSaveDialog.Filter = "pdf-Dateien|*.pdf|Alle Dateien|*.*";
         fileSaveDialog.AddExtension = true;
         fileSaveDialog.OverwritePrompt = false;   // This will be handled later
         if( fileSaveDialog.ShowDialog() == true )
         {
            txtOutputFile.Text = fileSaveDialog.FileName;
            lblStatus.Content = $"Zieldatei gewählt";
         }
         else { lblStatus.Content = "Auswahl abgebrochen"; }
      }

      private void txtFile_DragEnter( object sender, DragEventArgs e )
      {
         if( e.Data.GetDataPresent( DataFormats.FileDrop ) )
         {
            e.Effects = DragDropEffects.Copy;
         }
      }

      private void txtFile_DragDrop( object sender, DragEventArgs e )
      {
         string[] files = e.Data.GetData( DataFormats.FileDrop, true ) as string[];
         if( files != null && files.Length > 0 )
         {
            var firstDroppedFile = files.First();
            if( System.IO.Path.GetExtension( firstDroppedFile ) == ".pdf" )
            {
               ( sender as TextBox ).Text = firstDroppedFile;
            }
         }
      }

      private void Window_Closing( object sender, System.ComponentModel.CancelEventArgs e )
      {
         // Save window position
         Settings.Default.MainForm_PositionAndState = this.GetPlacement();

         // Save last used files
         Settings.Default.LastFrontPagesFile = txtFileFrontPages.Text;
         Settings.Default.LastBackPagesFile = txtFileBackPages.Text;
         Settings.Default.LastOutputFile = txtOutputFile.Text;

         // Save checkbox states
         Settings.Default.RotateBackPages = chkRotateBackPages.IsChecked ?? false;
         Settings.Default.DeleteSourceFiles = chkDeleteSourceFiles.IsChecked ?? false;

         // Actually save :)
         Settings.Default.Save();
      }

      private void mainForm_SourceInitialized( object sender, EventArgs e )
      {
         // Restore window position
         this.SetPlacement( Settings.Default.MainForm_PositionAndState );

         // Restore last used files
         txtFileFrontPages.Text = Settings.Default.LastFrontPagesFile;
         txtFileBackPages.Text = Settings.Default.LastBackPagesFile;
         txtOutputFile.Text = Settings.Default.LastFrontPagesFile;

         // Restore checkbox states
         chkRotateBackPages.IsChecked = Settings.Default.RotateBackPages;
         chkDeleteSourceFiles.IsChecked = Settings.Default.DeleteSourceFiles;
      }
   }
}
