﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.VisualBasic.FileIO;

using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;

namespace MMS.BrotherDuplexScan
{
   internal class PdfCombiner
   {
      public string FrontPagesPdf { get; internal set; }
      public string BackPagesPdf { get; internal set; }
      public string OutputFilePath { get; internal set; }
      public bool RotateBackPages { get; internal set; }
      public bool DeleteSourceFiles { get; internal set; }

      internal void SaveCombinedPdf()
      {
         CheckPrerequisites();

         var frontPagesFile = PdfReader.Open( FrontPagesPdf, PdfDocumentOpenMode.Import );
         var backPagesFile = PdfReader.Open( BackPagesPdf, PdfDocumentOpenMode.Import );

         if( frontPagesFile.PageCount != backPagesFile.PageCount )
         {
            throw new InvalidOperationException( "Front and back file page count must be equal!" );
         }

         var outputFile = new PdfDocument();

         var pageCount = frontPagesFile.PageCount;
         for( int i = 0; i < pageCount; i++ )
         {
            outputFile.AddPage( frontPagesFile.Pages[ i ] );

            var backPage = backPagesFile.Pages[ pageCount - 1 - i ];
            if( RotateBackPages ) { backPage.Rotate += 180; } 

            outputFile.AddPage( backPage );
         }

         outputFile.Save( OutputFilePath );
         
         if( DeleteSourceFiles )
         {
            // Delete to recycle bin, just to make sure we don't destroy data :)
            FileSystem.DeleteFile( FrontPagesPdf, UIOption.OnlyErrorDialogs, RecycleOption.SendToRecycleBin );
            FileSystem.DeleteFile( BackPagesPdf, UIOption.OnlyErrorDialogs, RecycleOption.SendToRecycleBin );
         }
      }

      private void CheckPrerequisites()
      {
         if( string.IsNullOrEmpty( OutputFilePath ) )
         {
            throw new InvalidOperationException( $"{nameof( OutputFilePath )} can not be empty!" );
         }
         if( !File.Exists( FrontPagesPdf ) )
         {
            throw new InvalidOperationException( $"{nameof( FrontPagesPdf )} must exist!" );
         }
         if( !File.Exists( BackPagesPdf ) )
         {
            throw new InvalidOperationException( $"{nameof( BackPagesPdf )} must exist!" );
         }
      }
   }
}
